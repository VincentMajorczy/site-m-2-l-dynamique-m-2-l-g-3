<?php
class clubDAO{

    public static function getClubsByLigue($unIdLigue){
        $result = [];
        $requeteprera = DBConnex::getInstance()->prepare("SELECT club.IDCLUB, club.NOMCLUB, club.ADRESSECLUB FROM club, ligue WHERE club.IDLIGUE = :idLigue AND club.IDLIGUE = ligue.IDLIGUE ORDER BY NOMLIGUE ");
        $requeteprera->bindParam(":idLigue", $unIdLigue);
        $requeteprera->execute();
        $ListeClubs = $requeteprera->fetchAll(PDO::FETCH_ASSOC);
        if(!empty($ListeClubs)){
            foreach($ListeClubs as $club){
                $unClub = new Club(null, null, null, null, null);
                $unClub->hydrate($club);
                $result[] = $unClub;
            }
        }
        return $result;
    }      
}