<?php
class UtilisateurDAO{
    
    public static function verification(string $login, string $mdp){
        
        $requetePrepa = dBConnex::getInstance()->prepare("select login from utilisateur where login = :login and  mdp = :mdp");
        $requetePrepa->bindParam( ":login", $login);
        $requetePrepa->bindParam( ":mdp" ,  $mdp);
        
       $requetePrepa->execute();

       $login = $requetePrepa->fetch();
       return $login[0];
    }

    public static function LesIntervenants(){
        $result = [];
        $requetePrepa = dBConnex::getInstance()->prepare("select utilisateur.IDUSER,utilisateur.IDFONCTION,utilisateur.IDLIGUE, 
        utilisateur.IDCLUB,utilisateur.NOM,utilisateur.PRENOM,utilisateur.STATUT,ligue.NOMLIGUE,club.NOMCLUB,fonction.LIBELLE 
        from utilisateur, ligue, club, fonction 
        WHERE utilisateur.IDLIGUE=ligue.IDLIGUE 
        AND utilisateur.IDCLUB=club.IDCLUB 
        AND utilisateur.IDFONCTION=fonction.IDFONCTION 
        order by NOM; " );
       
        $requetePrepa->execute();
        $liste = $requetePrepa->fetchAll(PDO::FETCH_ASSOC); 
    
        if(!empty($liste)){
            foreach($liste as $intervenant){
                $unIntervenant = new UtilisateurDTO(null,null,null,null,null,null,null,null,null,null,null,null);
                $unIntervenant->hydrate($intervenant);
                $result[] = $unIntervenant;
            }
        }
        return $result;
    }



    public static function getFonctionbyLogin($unLogin){                

        $requeteprera = dBConnex::getInstance()->prepare("select LIBELLE from fonction, utilisateur where LOGIN = :unLogin and utilisateur.IDFONCTION = fonction.IDFONCTION");

        $requeteprera->bindParam( ":unLogin", $unLogin);    
        
        $requeteprera->execute();

       $login = $requeteprera->fetch();
       return $login[0];
    }
    
    
    // public static function LesLigues(){
    //     $result = [];
    //     $requetePrepa = dBConnex::getInstance()->prepare("select * from ligue order by NOMLIGUE " );
       
    //     $requetePrepa->execute();
    //     $liste = $requetePrepa->fetchAll(PDO::FETCH_ASSOC); 

    //     if(!empty($liste)){
    //         foreach($liste as $ligue){
    //             $uneLigue = new ligueDTO(null,null,null,null);
    //             $uneLigue->hydrate($ligue);
    //             $result[] = $uneLigue;
    //         }
    //     }
    //     return $result;
    // }

    public static function UtilisateurSupprimer($IDUSER){
        $requetePrepa=DBConnex::getInstance()->prepare("DELETE FROM utilisateur WHERE IDUSER=:IDUSER");
        $requetePrepa->bindParam(':IDUSER', $IDUSER);
        return $requetePrepa->execute();
    }

    public static function UtilisateurAjouter($IDUSER,$IDFONCTION,$IDLIGUE,$IDCLUB,$nom, $prenom, $STATUT){
        $requetePrepa=DBConnex::getInstance()->prepare("INSERT INTO utilisateur (IDUSER, IDFONCTION ,IDLIGUE,IDCLUB,NOM, PRENOM,STATUT) VALUES  (:IDUSER,:IDFONCTION,:IDLIGUE,:IDCLUB,:nom, :prenom,:STATUT)");
        $requetePrepa->bindParam(':IDUSER', $IDUSER);
        $requetePrepa->bindParam(':IDFONCTION', $IDFONCTION);
        $requetePrepa->bindParam(':IDLIGUE', $IDLIGUE);
        $requetePrepa->bindParam(':IDCLUB', $IDCLUB);
        $requetePrepa->bindParam(':nom', $nom);
        $requetePrepa->bindParam(':prenom', $prenom);
        $requetePrepa->bindParam(':STATUT', $STATUT);



        $requetePrepa->execute();
    }

    // dernierID = U?

    // public static function lesContrats(){
    //     $result = [];
    //     $requetePrepa = dBConnex::getInstance()->prepare("select * from `contrat`  " );
       
    //     $requetePrepa->execute();
    //     $liste = $requetePrepa->fetchAll(PDO::FETCH_ASSOC); 
    
    //     if(!empty($liste)){
    //         foreach($liste as $intervenant){
    //             $unIntervenant = new contratDTO(null,null,null,null,null,null);
    //             $unIntervenant->hydrate($intervenant);
    //             $result[] = $unIntervenant;
    //         }
    //     }
    //     return $result;
    // }


    // public static function lesBulletins(){
    //     $result = [];
    //     $requetePrepa = dBConnex::getInstance()->prepare("select * from `bulletin`  " );
       
    //     $requetePrepa->execute();
    //     $liste = $requetePrepa->fetchAll(PDO::FETCH_ASSOC); 
    
    //     if(!empty($liste)){
    //         foreach($liste as $intervenant){
    //             $unIntervenant = new bulletinDTO(null,null,null,null,null);
    //             $unIntervenant->hydrate($intervenant);
    //             $result[] = $unIntervenant;
    //         }
    //     }
    //     return $result;
    // }

    // public static function UtilisateurEnregistrer($IDUSER,$IDFONCTION,$IDLIGUE,$IDCLUB,$nom, $prenom, $STATUT,$idutilisateur){
        
    //     $requetePrepa = DBConnex::getInstance()->prepare("UPDATE utilisateur SET IDUSER=:IDUSER,IDFONCTION=:IDFONCTION,IDLIGUE=:IDLIGUE,IDCLUB=:IDCLUB,NOM=:NOM,prenom=:prenom,STATUT=:STATUT WHERE idutilisateur=:idutilisateur");
    //     $requetePrepa->bindParam(':IDUSER', $IDUSER);
    //     $requetePrepa->bindParam(':IDFONCTION', $IDFONCTION);
    //     $requetePrepa->bindParam(':IDLIGUE', $IDLIGUE);
    //     $requetePrepa->bindParam(':IDCLUB', $IDCLUB);
    //     $requetePrepa->bindParam(':nom', $nom);
    //     $requetePrepa->bindParam(':prenom', $prenom);
    //     $requetePrepa->bindParam(':STATUT', $STATUT);


    //     return $requetePrepa->execute();

      
    // }

}



      
    
        
    
    


