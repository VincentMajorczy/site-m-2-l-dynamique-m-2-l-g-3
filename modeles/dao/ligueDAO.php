<?php
class ligueDAO{
    public static function ListeLigues(){
            $result = [];
            $requeteprera = DBConnex::getInstance()->prepare("select * from ligue ORDER BY NOMLIGUE");
            
            $requeteprera->execute();

        $listeLigues = $requeteprera->fetchAll(PDO::FETCH_ASSOC);
        if(!empty($listeLigues)){
            foreach($listeLigues as $ligue){
                $uneLigue = new Ligue(null, null, null, null);
                $uneLigue->hydrate($ligue);
                $uneLigue->setlesClubs(ClubDAO::getClubsByLigue($uneLigue->getIdLigue()));
                $result[] = $uneLigue;
            }
        }
        return $result;
    }
    
        public static function getLigueByNom(string $nomLigue){
            $result = [];
            $requeteprera = DBConnex::getInstance()->prepare("select * from ligue where NOMLIGUE =:nomLigue");
            $requeteprera->bindParam(":nomLigue", $nomLigue);
            $requeteprera->execute();

        $listeLigues = $requeteprera->fetchAll(PDO::FETCH_ASSOC);
        if(!empty($listeLigues)){
            foreach($listeLigues as $ligue){
                $uneLigue = new Ligue(null, null, null, null);
                $uneLigue->hydrate($ligue);

                $result[] = $uneLigue;
            }
        }
        return $result;
        }
            // Pour récupérer l'id d'une ligue sans avoir son avoir, en passant par son nom
        public static function getIdByNomLigue($nomLigue){
            $requeteprera = DBConnex::getInstance()->prepare("SELECT IDLIGUE from ligue where NOMLIGUE =:NOMLIGUE");

            $requeteprera->bindParam(":NOMLIGUE", $nomLigue);
            $requeteprera->execute();

            $listeLigues = $requeteprera->fetch(PDO::FETCH_ASSOC);
            
            return $listeLigues['IDLIGUE'];
        }
            // Fonction pour ajouter une ligue
        public static function ajouterLigue($idLigue, $nomLigue, $lienSite, $description){
            $requeteprera = DBConnex::getInstance()->prepare("INSERT INTO ligue (IDLIGUE, NOMLIGUE, LIENSITE, DESCRIPTIF) VALUES (:IDLIGUE,:NOMLIGUE,:LIENSITE,:DESCRIPTIF)");

            $requeteprera->bindParam(":IDLIGUE", $idLigue);
            $requeteprera->bindParam(":NOMLIGUE", $nomLigue);
            $requeteprera->bindParam(":LIENSITE", $lienSite);
            $requeteprera->bindParam(":DESCRIPTIF", $description);
            $requeteprera->execute();
        }
                    // Fonction pour enregistrer une ligue
        public static function enregistrerLigue($nomLigue,$lienSite,$description){
            $result = [];
            $requeteprera = DBConnex::getInstance()->prepare("INSERT INTO ligue (NOMLIGUE, LIENSITE, DESCRIPTIF) VALUES (:NOMLIGUE,:LIENSITE,:DESCRIPTIF)");

            $requeteprera->bindParam(":NOMLIGUE", $nomLigue);
            $requeteprera->bindParam(":LIENSITE", $nomLlienSiteigue);
            $requeteprera->bindParam(":DESCRIPTIF", $description);
            $requeteprera->execute();

            $listeLigues = $requeteprera->fetchAll(PDO::FETCH_ASSOC);
            if(!empty($listeLigues)){
                foreach($listeLigues as $ligue){
                    $uneLigue = new Ligue(null, null, null, null);
                    $uneLigue->hydrate($ligue);

                    $result[] = $uneLigue;
                }
            }
        return $result;
        }
            // Fonction pour modifier une ligue
        public static function modifierLigue($nomLigue, $lienSite, $description){
            
            $requeteprera = DBConnex::getInstance()->prepare("UPDATE ligue SET NOMLIGUE=:nomligue , LIENSITE=:liensite, DESCRIPTIF=:descriptif");
            $requeteprera->bindParam(":nomligue", $nomLigue);
            $requeteprera->bindParam(":liensite", $lienSite);
            $requeteprera->bindParam(":descriptif", $description);
            $requeteprera->execute();
            }

        public static function supprimerLigue($idLigue){
          
            $requeteprera = DBConnex::getInstance()->prepare("DELETE from ligue where IDLIGUE =:idLigue");
            $requeteprera->bindParam(":idLigue", $idLigue);
            $requeteprera->execute();
        }



     
}