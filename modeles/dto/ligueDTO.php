<?php
class ligueDTO{
	use Hydrate;
	private ?string $idligue ; 
	private ?string $nomLigue ; 
	private ?string $site ; 
	private ?string $descriptif ; 

	public function __construct(?string $uneidligue,?string $uneligue,?string $unsite,?string $undescriptif){
		$this->idligue = $uneidligue;
		$this->nomLigue = $uneligue;
		$this->site = $unsite;
		$this->descriptif = $undescriptif;
	}

	
    public function getIdligue() {
		return $this->idligue;
	}   
	
	public function setIdligue( ?string $uneidligue)  {
	    $this->idligue =  $uneidligue;
	}


	public function getNomLigue() {
		return $this->nomLigue;
	}   
	
	public function setnNomLigue( ?string $uneligue)  {
	    $this->nomLigue =  $uneligue;
	}


	public function getSite() {
		return $this->site;
	}   
	
	public function setSite( ?string $unsite)  {
	    $this->site =  $unsite;
	}


	public function getDescriptif() {
		return $this->idligue;
	}   
	
	public function setDescriptif( ?string $uneidligue)  {
	    $this->idligue =  $uneidligue;
	}
	

	
}