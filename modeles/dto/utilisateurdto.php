<?php
class UtilisateurDTO{
    use Hydrate;
    private ?string $idUser;
    private ?string $idFonction;
    private ?string $idLigue;
    private ?string $idClub;
   
    private ?string $nom;
    private ?string $prenom;
    private ?string $login;
    private ?string $mdp;
    private ?string $statut;

    /**
     * @param string|null $idUser
     * @param string|null $idFonction
     * @param string|null $idLigue
     * @param string|null $idClub
     * @param string|null $nom
     * @param string|null $prenom
     * @param string|null $login
     * @param string|null $mdp
     * @param string|null $statut
     */
    public function __construct(?string $idUser, ?string $idFonction, ?string $idLigue, ?string $idClub, ?string $nom, ?string $prenom, ?string $login, ?string $mdp, ?string $statut)
    
    {
        $this->idUser = $idUser;
        $this->idFonction = $idFonction;
        $this->idLigue = $idLigue;
        $this->idClub = $idClub;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->login = $login;
        $this->mdp = $mdp;
        $this->statut = $statut;
       
    }

    public function getIdUser(): ?string
    {
        return $this->idUser;
    }

    public function setIdUser(?string $idUser): void
    {
        $this->idUser = $idUser;
    }

    public function getIdFonction(): ?string
    {
        return $this->idFonction;
    }

    public function setIdFonction(?string $idFonction): void
    {
        $this->idFonction = $idFonction;
    }

    public function getIdLigue(): ?string
    {
        return $this->idLigue;
    }

    public function setIdLigue(?string $idLigue): void
    {
        $this->idLigue = $idLigue;
    }

    public function getIdClub(): ?string
    {
        return $this->idClub;
    }

    public function setIdClub(?string $idClub): void
    {
        $this->idClub = $idClub;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): void
    {
        $this->nom = $nom;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(?string $prenom): void
    {
        $this->prenom = $prenom;
    }

    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(?string $login): void
    {
        $this->login = $login;
    }

    public function getMdp(): ?string
    {
        return $this->mdp;
    }

    public function setMdp(?string $mdp): void
    {
        $this->mdp = $mdp;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(?string $statut): void
    {
        $this->statut = $statut;
    }



}
