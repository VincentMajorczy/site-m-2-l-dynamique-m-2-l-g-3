<?php
class Ligues{
	use hydrate;
	private array $ligues ;

	public function __construct($array){
		if (is_array($array)) {
			$this->ligues = $array;
		}
	}

	public function getNomLigue(){
		return $this->ligues;
	}

	public function getLienSite(){
		return $this->ligues;
	}

	public function getListeLigues(){
		 
		return $this->ligues;
	}


	/*public function ListeClubsByLigue($unIdClub){
		 
		$i = 0;
		while ($unIdClub != $this->club[$i]->getIdClub() && $i < count($this->club)-1){
			$i++;
		}
		if ($unIdClub == $this->club[$i]->getIdClub()){
			return $this->club[$i];
		}
	}
	*/


	


	public static function ajouterLigue($nomLigue, $lienSite, $description) {
		try {
			// Préparation de la requête SQL sans les guillemets autour des paramètres
			$requetePreparee = DBConnex::getInstance()->prepare("INSERT INTO ligue (NOMLIGUE, LIENSITE, DESCRIPTIF) VALUES (:NOMLIGUE, :LIENSITE, :DESCRIPTIF)");
	
			// Liaison des paramètres
			$requetePreparee->bindParam(":NOMLIGUE", $nomLigue);
			$requetePreparee->bindParam(":LIENSITE", $lienSite);
			$requetePreparee->bindParam(":DESCRIPTIF", $description);
	
			// Exécution de la requête SQL
			$requetePreparee->execute();
	
			// Vous pouvez retourner un message de succès ou true si nécessaire
			return "Ligue ajoutée avec succès.";
		} catch (PDOException $e) {
			// Gérer les erreurs ici (par exemple, enregistrer dans un journal)
			// Vous pouvez aussi renvoyer une erreur ou une exception si vous le souhaitez
			return "Erreur lors de l'ajout de la ligue : " . $e->getMessage();
		}
	}
	
	public function deleteLigue($unIdLigue): bool {
        // Recherchez la ligue dans le tableau des ligues associées.
        $key = array_search($unIdLigue, array_column($this->ligues, 'IDLIGUE'));

        if ($key !== false) {
            // Supprimez la ligue du tableau.
            unset($this->ligues[$key]);
            // Réindexez le tableau pour éviter les clés manquantes.
            $this->ligues = array_values($this->ligues);
            return true; // Indique que la suppression a réussi.
        }

        return false; // Indique que la ligue n'a pas été trouvée.
    }

	
	
}