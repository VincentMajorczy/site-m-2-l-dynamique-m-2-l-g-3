<?php
class utilisateurDTO{
    use Hydrate;
	private ?string  $idUser;
	private ?string  $idFonction;
    private ?string  $idLigue;
    private ?string  $idClub;
    private ?string  $nom;
    private ?string  $prenom;
    private ?string  $login;
    private ?string  $mdp; 
    private ?string  $statut; 
	private ?string  $nomLigue; 
	private ?string  $nomClub; 
	private ?string  $libelle;



	public function __construct(?string $unidUser  , ?string $unidFonction, ?string $unidLigue, ?string $unidClub, ?string $unnom,  
   ?string $unprenom, ?string $unlogin, ?string $unmdp, ?string $unStatut, ?string $unNomLigue,  ?string $unNomClub, ?string $unLibelle){
		$this->idUser = $unidUser;
		$this->idFonction = $unidFonction;
        $this->idLigue = $unidLigue;
        $this->idClub = $unidClub;
        $this->nom = $unnom;
        $this->prenom = $unprenom;
        $this->login = $unlogin;
        $this->mdp = $unmdp;
        $this->statut = $unStatut;
		$this->nomLigue = $unNomLigue;
		$this->nomClub = $unNomClub;
		$this->libelle = $unLibelle;
	}


    public function getIdUser() {
		return $this->idUser;
	}   
	
	public function setIdUser(?string $unidUser)  {
	    $this->idUser =  $unidUser;
	}


	public function getIdFonction() {
		return $this->idFonction;
	}
	
	public function setIdFonction(?string $unIdFonction)  {
	    $this->idFonction =  $unIdFonction;
	}

    public function getIdLigues() {
		return $this->idLigue;
	}
	
	public function setIdLigues(?string $unidLigue)  {
	    $this->idLigue =  $unidLigue;
	}

	public function getIdClub() {
		return $this->idClub;
	}
	
	public function setIdClub(?string $unidClub)  {
	    $this->idClub =  $unidClub;
	}

    public function getNom(){
		return $this->nom;
	}
	
	public function setNom(?string $unnom)  {
	    $this->nom =  $unnom;
	}

    public function getPrenom() {
		return $this->prenom;
	}
	
	public function setPrenom(?string $unprenom)  {
	    $this->prenom =  $unprenom;
	}

    public function getLogin() {
		return $this->login;
	}
	
	public function setLogin(?string $unlogin)  {
	    $this->login =  $unlogin;
	}

    public function getmdp() {
		return $this->mdp;
	}
	
	public function setmdp(?string $unmdp)  {
	    $this->mdp =  $unmdp;
	}

    public function getStatut() {
		return $this->statut;
	}
	
	public function setStatut(?string $unStatut)  {
	    $this->statut =  $unStatut;
	}

	public function getNomLigue() {
		return $this->nomLigue;
	}
	
	public function setNomLigue(?string $unNomLigue)  {
	    $this->nomLigue =  $unNomLigue;
	}

	public function getNomClub() {
		return $this->nomClub;
	}
	
	public function setNomClub(?string $unNomClub)  {
	    $this->nomClub =  $unNomClub;
	}

	public function getLibelle() {
		return $this->libelle;
	}
	
	public function setLibelle(?string $unLibelle)  {
	    $this->libelle =  $unLibelle;
	}

	


	

	
}
?>