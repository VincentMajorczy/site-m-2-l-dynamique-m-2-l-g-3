<?php
class Club{
    use Hydrate;
	private ?string $idClub;
	private ?string $idLigue;
	private ?string $idCommune;
	private ?string $nomClub;
	private ?string $adresseClub;
	
	public function __construct(?string $unIdClub  , ?string $unIdLigue , ?string $unIdCommune , ?string $unNomClub , ?string $uneAdresseClub){
	    $this->idClub = $unIdClub;
	    $this->idLigue = $unIdLigue;
	    $this->idCommune = $unIdCommune;
	    $this->nomClub = $unNomClub;
	    $this->adresseClub = $uneAdresseClub;
	}
	
    // GETTERS
    
	public function getIdClub(): string{
	    return $this->idClub;
	}

    public function getIdLigue(): string{
	    return $this->idLigue;
	}

    public function getIdCommune(): string{
	    return $this->idCommune;
	}

    public function getNomClub(): string
    {
        return $this->nomClub;
    }

    public function getAdresseClub(): string
    {
        return $this->adresseClub;
    }
    
    // SETTERS
    
    public function setIdClub(string $unIdClub): void{
        $this->idClub =  $unIdClub;
    }
    
    public function setIdLigue(string $unIdLigue): void{
        $this->idLigue = $unIdLigue;
    }
    
    public function setIdCommune(string $unIdCommune)
    {
        $this->idCommune = $unIdCommune;
    }

    public function setNomClub(string $unNomClub)
    {
        $this->nomClub = $unNomClub;
    }

    public function setAdresseClub(string $uneAdresseClub)
    {
        $this->adresseClub = $uneAdresseClub;
    }
}