<?php
class contratDTO{
    use Hydrate;
	private ?string  $IdContrat;
	private ?string  $IdUserContrat;
    private ?string  $DateDebut;
    private ?string  $DateFin;
    private ?string  $TypeContrat;
    private ?int  $NbHeures;

    public function __construct(?string $unIdContrat, ?string $unIdUser, ?string $uneDateDebut, ?string $uneDateFin, ?string  $unTypeContrat, ?int  $NbHeures){
        $this->IdContrat=$unIdContrat; 
        $this->IdUserContrat=$unIdUser; 
        $this->DateDebut=$uneDateDebut; 
        $this->DateFin=$uneDateFin; 
        $this->TypeContrat=$unTypeContrat; 
        $this->NbHeures=$NbHeures; 
    }

    public function getIdContrat() {
		return $this->IdContrat;
	}   
	
	public function setIdContrat(?string $unIdContrat)  {
	    $this->IdContrat =  $unIdContrat;
	}



    public function getIdUserContrat() {
		return $this->IdUserContrat;
	}   
	
	public function setIdUserContrat(?string $unIdUser)  {
	    $this->IdUserContrat =  $unIdUser;
	}




    public function getDateDebut() {
		return $this->DateDebut;
	}   
	
	public function setDateDebut(?string $uneDateDebut)  {
	    $this->DateDebut =  $uneDateDebut;
	}



    public function getDateFin() {
		return $this->DateFin;
	}   
	
	public function setDateFin(?string $uneDateFin)  {
	    $this->DateFin =  $uneDateFin;
	}



    public function getTypeContrat() {
		return $this->TypeContrat;
	}   
	
	public function setTypeContrat(?string $unTypeContrat)  {
	    $this->TypeContrat =  $unTypeContrat;
	}



    public function getNbHeures() {
		return $this->NbHeures;
	}   
	
	public function setNbHeures(string $NbHeures)  {
	    $this->NbHeures =  $NbHeures;
	}
}

    

