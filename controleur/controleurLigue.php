<?php 
$formulaireLigue = new Formulaire('post', 'index.php', 'fLigue', 'fLigue');
$_SESSION['listeLigues'] = new ligues(ligueDAO::getLigueByNom($_SESSION["nomLigue"]));


if(UtilisateurDAO::getFonctionbyLogin($_SESSION["identification"]) == "responsable_formation"){
    $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerLabel('Identifiant :', "z"),1);
    $formulaireLigue->ajouterComposantTab();
}
    // Supprimer une ligue
if(isset($_POST['supprimerLigue'])){
    ligueDAO::supprimerLigue(ligueDAO::getIdByNomLigue($_SESSION["nomLigue"]));
}

if(isset($_POST['modifierLigue'])){
    ligueDAO::modifierLigue($_POST['NomLigue'], $_POST['LienSite'], $_POST['Descriptif'],);
}
if(!isset($_POST['modifierLigue'])){

    foreach ($_SESSION['listeLigues']->getListeLigues() as $uneLigue){
        $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerMessage("saut_de_ligne", "<br><br>"), 1);
        $formulaireLigue->ajouterComposantTab();
        $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerInputTexte("NomLigue", "NomLigue", $uneLigue->getNomLigue(), false, "Nom de la ligue", 1));
        $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerMessage("saut_de_ligne", "<br><br>"), 1);
        $formulaireLigue->ajouterComposantTab();
        $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerInputTexte("LienSite", "LienSite", $uneLigue->getLienSite(), false, "Lien du site web de la ligue", 1));
        $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerMessage("saut_de_ligne", "<br><br>"), 1);
        $formulaireLigue->ajouterComposantTab();
        $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerInputTexte("Descriptif", "Descriptif", $uneLigue->getDescriptif(), false, "Description de la ligue", 1));

            //Ajouter les clubs affiliés à une ligue grâce à l'ID
        $_SESSION['listeClubs']=new clubs(ClubDAO::getClubsByLigue($uneLigue->getIdLigue()));
        foreach ($_SESSION['listeClubs']->getListeClub() as $unClub){
            $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerMessage("ListeClub", $unClub->getNomClub()), 1);
            $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerMessage("saut_de_ligne", "<br><br>"), 1);
            $formulaireLigue->ajouterComposantTab();
            $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerMessage("ListeClub", $unClub->getAdresseClub()), 1);
            $formulaireLigue->ajouterComposantTab();
        }
        $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerMessage("saut_de_ligne", "<br><br>"), 1);
        $formulaireLigue->ajouterComposantTab();
            //Bouton Supprimer
        $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerInputSubmit("supprimerLigue","supprimerLigue","supprimer"));
        $formulaireLigue->ajouterComposantTab();
            //Bouton Modifier
        $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerInputSubmit("modifierLigue","modifierLigue","modifier"));
        $formulaireLigue->ajouterComposantTab();
    }
}
else{
        $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerMessage("saut_de_ligne", "<br><br>"), 1);
        $formulaireLigue->ajouterComposantTab();
        $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerInputTexte("NomLigue", "NomLigue", $uneLigue->getNomLigue(), 1, "Nom de la ligue", 0));
        $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerMessage("saut_de_ligne", "<br><br>"), 1);
        $formulaireLigue->ajouterComposantTab();
        $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerInputTexte("LienSite", "LienSite", $uneLigue->getLienSite(), 1, "Lien du site web de la ligue", 0));
        $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerMessage("saut_de_ligne", "<br><br>"), 1);
        $formulaireLigue->ajouterComposantTab();
        $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerInputTexte("Descriptif", "Descriptif", $uneLigue->getDescriptif(), 1, "Description de la ligue", 0));
}

$formulaireLigue->creerFormulaire($_SESSION['listeLigues']);

$formulaireLigue->creerFormulaire();
require_once "vue/vueLigue.php";