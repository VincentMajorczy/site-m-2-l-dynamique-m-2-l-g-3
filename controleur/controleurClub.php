<?php

$formulaireClub = new Formulaire('post', 'index.php', 'fClub', 'fClub');
$menuClub = new Menu("menuClub");

//$_SESSION['ListeClubsByLigue'] = new clubs(clubDAO::ListeClubsByLigue());

if(clubDAO::getFonctionbyLogin($_SESSION["identification"]) == "responsable_formation"){
    $formulaireClub->ajouterComposantLigne($formulaireClub->creerLabel('Identifiant :', "z"),1);
    $formulaireClub->ajouterComposantTab();
}

foreach ($_SESSION['listeLigues']->getListeLigues() as $uneLigue){
    $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerInputTexte("NomLigue", "NomLigue", $uneLigue->getNomClub(), "1", "", "0"), 1);
    $formulaireLigue->ajouterComposantTab();
    $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerInputTexte("LienSite", "LienSite", $uneLigue->getAdresseClub(), "1", "", "0"), 1);
    $formulaireLigue->ajouterComposantTab();
    $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerInputTexte("LienSite", "LienSite", $uneLigue->get(), "1", "", "0"), 1);
    $formulaireLigue->ajouterComposantTab();
}

$leMenuLigues = $formulaireLigue->creerFormulaire($_SESSION['listeLigues']);

$formulaireClub->creerFormulaire();
require_once 'vue/vueClub.php';



