<?php

$formulaireLigue = new Formulaire('post', 'index.php', 'fLigues', 'fLigues');
$_SESSION['listeLigues'] = new ligues(ligueDAO::ListeLigues());



if(UtilisateurDAO::getFonctionbyLogin($_SESSION["identification"]) == "responsable_formation"){
    $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerLabel('Identifiant :', "z"),1);
    $formulaireLigue->ajouterComposantTab();
}

if(!isset($_POST['ajouterLigue'])){
    foreach ($_SESSION['listeLigues']->getListeLigues() as $uneLigue){
        $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerInputSubmit("nomLigue","nomLigue",$uneLigue->getNomLigue()));
        $formulaireLigue->ajouterComposantTab();

    }
    $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerInputSubmit("ajouterLigue","ajouterLigue","ajouter"));
    $formulaireLigue->ajouterComposantTab();

    $leMenuLigues = $formulaireLigue->creerFormulaire($_SESSION['listeLigues']);
}
else{
    $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerInputTexte("textBoxIDLigue","textBoxIDLigue","", "1", "ID de la ligue","0"));
    $formulaireLigue->ajouterComposantTab();
    $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerInputTexte("textBoxNomLigue","textBoxNomLigue","", "1", "Nom de la ligue","0"));
    $formulaireLigue->ajouterComposantTab();
    $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerInputTexte("textBoxLienSite","textBoxLienSite","", "0", "Lien du site web ","0"));
    $formulaireLigue->ajouterComposantTab();
    $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerInputTexte("textBoxDescriptif","textBoxDescriptif","", "0", "Descriptif","0"));
    $formulaireLigue->ajouterComposantTab();
    $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerInputSubmit("annulerLigue","annulerLigue","annuler"));
    $formulaireLigue->ajouterComposantTab();
    $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerInputSubmit("enregistrerLigue","enregistrerLigue","enregistrer"));
    $formulaireLigue->ajouterComposantTab();
    
}



$formulaireLigue->creerFormulaire();
require_once 'vue/vueLigues.php' ;




/*if(UtilisateurDAO::getFonctionbyLogin($_SESSION["identification"]) == "secretaire"){
    $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerLabel('Nom :'),1);
    $formulaireLigue->ajouterComposantTab();

    $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerLabel('Lien vers le site : '),1);
    $formulaireLigue->ajouterComposantTab();

    $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerLabel('Présentation : '),1);
    $formulaireLigue->ajouterComposantTab();

    $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerLabel('Liste des clubs affiliés :'),1);
    $formulaireLigue->ajouterComposantTab();

    $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerLabel('Ajouter les informations :'),1);
    $formulaireLigue->ajouterComposantTab();

    $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerLabel('Modifier les informations :'),1);
    $formulaireLigue->ajouterComposantTab();

    $formulaireLigue->ajouterComposantLigne($formulaireLigue->creerLabel('Supprimer les informations :'),1);
    $formulaireLigue->ajouterComposantTab();

}*/