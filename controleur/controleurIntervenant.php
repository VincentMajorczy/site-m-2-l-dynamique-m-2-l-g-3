<?php
// echo $_SESSION['identification'];
$_SESSION['listeintervenant'] = new LesutilisateursDTO(UtilisateurDAO::LesIntervenants());
// $_SESSION['listeligue'] = new lesLiguesDTO(utilisateurDAO::LesLigues());


$menuIntervenant = new Menu("menuIntervannt");
// $menueLigue = new Menu("menueLigue");

foreach ($_SESSION['listeintervenant']->getIntervenant() as $unIntervanant){
    // var_dump($unIntervanant);
	$menuIntervenant->ajouterComposant($menuIntervenant->creerItemLien($unIntervanant->getNom(),1));
}
// foreach ($_SESSION['listeligue']->getLesLigues() as $uneLigue){
// 	$menueLigue->ajouterComposant($menueLigue->creerItemLien($uneLigue->getNomLigue(),1));
// }

if(isset($_GET['utilisateur'])){
	$_SESSION['utilisateur'] = $_GET['utilisateur'];
  
    
}
else
{
	if(!isset($_SESSION['utilisateur'])){
		$_SESSION['utilisateur']= 0;
       
	}
}
if(isset($_POST["ajouterUtilisateur"])){
    $_SESSION["utilisateur"]=0;

}


if(isset($_POST["anullerUtilisateur"])){
	$_SESSION["utilisateur"]=$_SESSION['listeintervenant']->premierUtilisateur();
}


$menuUtilisateur = new Menu("menuUtilisateur");
foreach ($_SESSION['listeintervenant']->getIntervenant() as $unIntervenant){
	$menuUtilisateur->ajouterComposant($menuUtilisateur->creerItemLien($unIntervenant->getIdUser() , $unIntervenant->getNom() . " " . $unIntervenant->getPrenom()));
    
}
$leMenuIntervenant = $menuUtilisateur->creerMenuUtilisateur($_SESSION['utilisateur']);

// $menuLigue = new Menu("menuUtilisateur");
// foreach ($_SESSION['listeligue']->getLesLigues() as $uneLigue){
// 	$menuLigue->ajouterComposant($menuLigue->creerItemLien($uneLigue->getIdLigue() , $uneLigue->getNomLigue()));
    
// }
// $leMenuIntervenant = $menuLigue->creerMenuUtilisateur($_SESSION['utilisateur']);


if(isset($_POST["supprimerUtilisateur"])){
	$reponseSGBD=utilisateurDAO::UtilisateurSupprimer($_SESSION['utilisateurActif']->getIdUser());
	if($reponseSGBD){
		$_SESSION['listeintervenant']=new LesutilisateursDTO(utilisateurDAO::LesIntervenants());
		$_SESSION['utilisateur']=$_SESSION['listeintervenant']->premierUtilisateur();
	}
	
}

$_SESSION["utilisateurActif"] = $_SESSION['listeintervenant']->chercheUtilisateur($_SESSION['utilisateur']);
// $_SESSION["ligueActif"] = $_SESSION['listeligue']->chercheLigue($_SESSION['utilisateur']);


// echo "<br>";

if(isset($_POST["ajouterUtilisateurBDD"])){
	$reponseSGBD = UtilisateurDAO::UtilisateurAjouter($_POST["idUser"],$_POST["idFonction"],$_POST["idLigue"],$_POST["idClub"],$_POST["nom"],$_POST["prenom"],$_POST["statut"]);
	if($reponseSGBD){
		$_SESSION['listeintervenant'] = new LesutilisateursDTO(utilisateurDAO::LesIntervenants());
	}
    else{
        echo "Error";
    }
}

// if(isset($_POST["enregistrerUtilisateur"])){
// 	$reponseSGBD = utilisateurDAO::UtilisateurEnregistrer($_POST["idUser"],$_POST["idFonction"],$_POST["idLigue"],$_POST["idClub"],$_POST["nom"],$_POST["prenom"],$_POST["statut"],$_SESSION['utilisateurActif']->getIdUser());
// 	if($reponseSGBD){
// 		$_SESSION['listeintervenant'] = new LesutilisateursDTO(utilisateurDAO::LesIntervenants());
// 	}
// }

$formulaireGestion = new formulaire('post', 'index.php', 'fIntervenant', 'fIntervenant');

if(utilisateurDAO::getFonctionbyLogin($_SESSION["identification"]) == "responsable_formation"){
    $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Identifiant :', "z"),1);
    $formulaireGestion->ajouterComposantTab();
}

if(utilisateurDAO::getFonctionbyLogin($_SESSION["identification"]) == "secretaire"){
    $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Identifiant :', "z"),1);
    $formulaireGestion->ajouterComposantTab();
}


if(utilisateurDAO::getFonctionbyLogin($_SESSION["identification"]) == "ressource_humaine"){
    //$formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel("Veuiller selectioner un intervenant: "),1);
    //$formulaireGestion->ajouterComposantTab();
   
    if ($_SESSION['utilisateur']!=0){

        if (isset($_POST["modifierUtilisateur"])){
            // var_dump($_SESSION["utilisateur"]);
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Nom Intervenant : '/*.$_SESSION["utilisateurActif"]->getNom()*/),1);
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("nom", "nom", $_SESSION["utilisateurActif"]->getNom(),"1" , "", "0") , 1 );
            $formulaireGestion->ajouterComposantTab();

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Prenom Intervenant : '/*.$_SESSION["utilisateurActif"]->getPrenom()*/),1);
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("prenom", "prenom",$_SESSION["utilisateurActif"]->getPrenom(),"1" , "", "0") , 1 );
            $formulaireGestion->ajouterComposantTab();

            

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Ligue Intervenant : ',),1);
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("nomLigue", "nomLigue",$_SESSION["utilisateurActif"]->getNomLigue(),"1" , "", "0") , 1 );
            $formulaireGestion->ajouterComposantTab();

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Club Intervenant : ',),1);
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("nomClub", "nomClub",$_SESSION["utilisateurActif"]->getNomClub(),"1" , "", "0") , 1 );
            $formulaireGestion->ajouterComposantTab();

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Fonction : ',),1);
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("libelle", "libelle",$_SESSION["utilisateurActif"]->getLibelle(),"1" , "", "0") , 1 );
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Statut : ',),1);
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("statut", "statut",$_SESSION["utilisateurActif"]->getStatut(),"1" , "", "0") , 1 );
            $formulaireGestion->ajouterComposantTab();

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Contrat : ',),1);
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("contrat", "contrat","1" , "", "0") , 1 );
            $formulaireGestion->ajouterComposantTab();

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputSubmit("enregistrerUtilisateur","enregistrerUtilisateur","Enregistrer"));
            $formulaireGestion->ajouterComposantTab();  

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputSubmit("anullerUtilisateur","anullerUtilisateur","Annuler"))  ;
            $formulaireGestion->ajouterComposantTab(); 
        }
        else{
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Nom Intervenant : '/*.$_SESSION["utilisateurActif"]->getNom()*/),1);
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("nom", "nom", $_SESSION["utilisateurActif"]->getNom(),"1" , "", "1") , 1 );
            $formulaireGestion->ajouterComposantTab();

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Prenom Intervenant : '/*.$_SESSION["utilisateurActif"]->getPrenom()*/),1);
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("prenom", "prenom",$_SESSION["utilisateurActif"]->getPrenom(),"1" , "", "1") , 1 );
            $formulaireGestion->ajouterComposantTab();

           

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Ligue Intervenant : ',),1);
            // $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("nomLigue", "nomLigue",$_SESSION["utilisateurActif"]->getNomLigue(),"1" , "", "1") , 1 );
            $formulaireGestion->ajouterComposantTab();

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Club Intervenant : ',),1);
            // $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("nomClub", "nomClub",$_SESSION["utilisateurActif"]->getNomClub(),"1" , "", "1") , 1 );
            $formulaireGestion->ajouterComposantTab();

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Fonction : ',),1);
            // $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("libelle", "libelle",$_SESSION["utilisateurActif"]->getLibelle(),"1" , "", "1") , 1 );
            $formulaireGestion->ajouterComposantTab();

             $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Statut Intervenant : '/*.$_SESSION["utilisateurActif"]->getStatut()*/),1);
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("prenom", "prenom",$_SESSION["utilisateurActif"]->getStatut(),"1" , "", "1") , 1 );
            $formulaireGestion->ajouterComposantTab();  

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputSubmit("modifierUtilisateur","ModifierUtilisateur","Modifier"));
            $formulaireGestion->ajouterComposantTab();  

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputSubmit("ajouterUtilisateur","AjouterUtilisateur","Ajouter"));
            $formulaireGestion->ajouterComposantTab(); 

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputSubmit("supprimerUtilisateur","SupprimerUtilisateur","Supprimer"))  ;
            $formulaireGestion->ajouterComposantTab(); 
        }
         
       
    }   
    else{
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel("Veuillez remplir les champs pour crée un utilisateur :  "),1);
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel("Id de l'intervenant : "),1);
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("idUser", "idUser", "","0" , "", "0") , 1 );
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('ID Fonction : ',),1);
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte(" idFonction", "idFonction","","0" , "", "0") , 1 );
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('ID Ligue : ',),1);
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("idLigue", "idLigue","","0" , "", "0") , 1 );
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('ID Club : ',),1);
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("idClub", "idClub","","0" , "", "0") , 1 );
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Nom Intervenant : '),1);
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("nom", "nom", "","0" , "", "0") , 1 );
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Prenom Intervenant : '),1);
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("prenom", "prenom", "","0" , "", "0") , 1 );
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Statut : '),1);
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("statut", "statut","","0" , "", "0") , 1 );
        $formulaireGestion->ajouterComposantTab();
        
        // $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Statut Intervenant : '/*.$_SESSION["utilisateurActif"]->getStatut()*/),1);
        // $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("prenom", "prenom","","0" , "", "0") , 1 );
        // $formulaireGestion->ajouterComposantTab();  

        // $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Ligue Intervenant : ',),1);
        // $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("nomLigue", "nomLigue","","0" , "", "0") , 1 );
        // $formulaireGestion->ajouterComposantTab();

        // $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Club Intervenant : ',),1);
        // $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("nomClub", "nomClub","","0" , "", "0") , 1 );
        // $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputSubmit("ajouterUtilisateurBDD","AjouterUtilisateurBDD","Ajouter"));
        $formulaireGestion->ajouterComposantTab(); 
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputSubmit("anullerUtilisateur","anullerUtilisateur","Annuler"))  ;
        $formulaireGestion->ajouterComposantTab(); 
    }

    // if(isset($_POST['ajouterUtilisateurBDD'])){
    //     utilisateurDAO::UtilisateurAjouter($_POST['nom'], $_POST['prenom']);
    // }

    
    
}

$formulaireGestion->creerFormulaire();

require_once 'vue/vueIntervenant.php';